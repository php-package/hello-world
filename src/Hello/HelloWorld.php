<?php

declare(strict_types=1);

namespace PhpPackage\Hello;

final class HelloWorld
{
    public function __construct(private string $name = 'everyone') {}

    public function getName(): string
    {
        return $this->name;
    }

    public function hello(): string
    {
        return "Hello, {$this->name}! Nice to meet you.";
    }
}

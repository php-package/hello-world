<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use PhpPackage\Hello\HelloWorld;

$helloWorld = new HelloWorld('Stephen');

echo $helloWorld->hello() . PHP_EOL;
